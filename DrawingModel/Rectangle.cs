﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Rectangle : Shape
    {
        List<Point> _allPoint;
        public Rectangle()
        {

        }

        public Rectangle(Rectangle rectangle)
        {
            _x1 = rectangle._x1;
            _x2 = rectangle._x2;
            _y1 = rectangle._y1;
            _y2 = rectangle._y2;
            _highlight = rectangle._highlight;
        }

        // Draw rectangle abstract class
        public override void Draw(IGraphics graphics)
        {
            GetAllPoint();
            graphics.DrawRectangle(_allPoint, _highlight);
            graphics.DrawString(String.Empty);
            graphics.DrawString(String.Empty);
            if (_highlight)
            {
                CheckPoint();
                graphics.DrawString(Constant.RECTANGLE_OUTPUT + this._x1 + Constant.COLON + this._y1 + Constant.COLON + this._x2 + Constant.COLON + this._y2 + Constant.END);
            }
        }

        // check point instde shape
        public override bool ClickCheck(double x1, double y1)
        {
            GetAllPoint();
            int index = 0;
            int lastIndex = 0;
            bool isPassed = false;
            for (index = 0, lastIndex = Constant.NUMBER_FOUR - 1; index < Constant.NUMBER_FOUR; lastIndex = index++)
            {
                if (((_allPoint[index].pointY > y1) != (_allPoint[lastIndex].pointY > y1)) &&
                    (x1 < (_allPoint[lastIndex].pointX - _allPoint[index].pointX) * (y1 - _allPoint[index].pointY) / (_allPoint[lastIndex].pointY - _allPoint[index].pointY) + _allPoint[index].pointX))
                    isPassed = !isPassed;
            }
            return isPassed;
        }

        // Get all point function
        private void GetAllPoint()
        {
            float topX = (this._x2 > this._x1) ? (float)this._x1 : (float)this._x2;
            float bottomX = (this._x2 > this._x1) ? (float)this._x2 : (float)this._x1;
            float topY = (this._y2 > this._y1) ? (float)this._y1 : (float)this._y2;
            float bottomY = (this._y2 > this._y1) ? (float)this._y2 : (float)this._y1;
            _allPoint = new List<Point>();
            _allPoint.Add(new Point(topX, topY));
            _allPoint.Add(new Point(bottomX, topY));
            _allPoint.Add(new Point(bottomX, bottomY));
            _allPoint.Add(new Point(topX, bottomY));
        }

        // Clone Shape
        public override Shape Clone()
        {
            return new Rectangle(this);
        }
    }
}
