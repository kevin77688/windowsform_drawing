﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace DrawingModel
{
    public class CloudManage
    {
        public void SaveFile(CommandManager commandManager, List<Shape> shapes, Stack<List<Shape>> clearShapes)
        {
            //FileStream fileStream = new FileStream("Drawing.bin", FileMode.Create);
            //BinaryFormatter binaryFormatter = new BinaryFormatter();
            //Data data = new Data(commandManager, shapes, clearShapes);
            //binaryFormatter.Serialize(fileStream, data);
            //fileStream.Close();
            Data data = new Data(commandManager, shapes, clearShapes);
            XmlSerializer xmlSerializer = new XmlSerializer(data.GetType());
            TextWriter writer = new StreamWriter("Drawing");
            xmlSerializer.Serialize(writer, data);
            writer.Close();



        }
    }
}
