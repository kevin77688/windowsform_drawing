﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public abstract class Shape
    {
        public double _x1
        {
            get;set;
        }

        public double _x2
        {
            get;set;
        }

        public double _y1
        {
            get;set;
        }

        public double _y2
        {
            get;set;
        }

        public bool _highlight
        {
            get; set;
        } = false;

        // Shape abstract class SetFirstPoint
        public void SetFirstPoint(double x1, double y1)
        {
            _x1 = x1;
            _y1 = y1;
        }

        public Point GetFirstPoint()
        {
            return new Point(_x1, _y1);
        }

        public Point GetSecondPoint()
        {
            return new Point(_x2, _y2);
        }

        // Shape abstract class SetSecondPoint
        public void SetSecondPoint(double x2, double y2)
        {
            _x2 = x2;
            _y2 = y2;
        }

        // Shape abstract class Draw
        public abstract void Draw(IGraphics graphics);

        // check point instde shape
        public abstract bool ClickCheck(double x1, double y1);

        // clone new shape
        public abstract Shape Clone();

        public void CheckPoint()
        {
            if (_x2 < _x1)
            {
                var temp = _x2;
                _x2 = _x1;
                _x1 = temp;
            }

            if (_y2 < _y1)
            {
                var temp = _y2;
                _y2 = _y1;
                _y1 = temp;
            }
        }

        public bool IsInDistance(double pointX, double pointY)
        {
            if (pointX < _x1 || pointY < _y1)
                return false;
            var distance = Math.Pow(_x2 - pointX, Constant.NUMBER_TWO) + Math.Pow(_y2 - pointY, Constant.NUMBER_TWO);
            distance = Math.Sqrt(distance);
            return distance < Constant.NUMBER_FIVE;
        }
    }
}
