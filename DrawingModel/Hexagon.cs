﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Hexagon : Shape
    {
        List<Point> _allPoint;

        public Hexagon()
        {

        }

        public Hexagon(Hexagon hexagon)
        {
            _x1 = hexagon._x1;
            _x2 = hexagon._x2;
            _y1 = hexagon._y1;
            _y2 = hexagon._y2;
            _highlight = hexagon._highlight;
        }

        // Draw line abstract class
        public override void Draw(IGraphics graphics)
        {
            GetAllPoint();
            graphics.DrawHexagon(_allPoint, _highlight);
            graphics.DrawString(String.Empty);
            if (_highlight)
            {
                CheckPoint();
                graphics.DrawString(Constant.HEXAGON_OUTPUT + this._x1 + Constant.COLON + this._y1 + Constant.COLON + this._x2 + Constant.COLON + this._y2 + Constant.END);
            }
        }

        // check point instde shape
        public override bool ClickCheck(double x1, double y1)
        {
            GetAllPoint();
            int index = 0;
            int lastIndex = 0;
            bool isPassed = false;
            for (index = 0, lastIndex = Constant.NUMBER_SIX - 1; index < Constant.NUMBER_SIX; lastIndex = index++)
            {
                if (((_allPoint[index].pointY > y1) != (_allPoint[lastIndex].pointY > y1)) &&
                    (x1 < (_allPoint[lastIndex].pointX - _allPoint[index].pointX) * (y1 - _allPoint[index].pointY) / (_allPoint[lastIndex].pointY - _allPoint[index].pointY) + _allPoint[index].pointX))
                    isPassed = !isPassed;
            }
            return isPassed;
        }

        // Get all point function
        private void GetAllPoint()
        {
            float topX = (_x2 > _x1) ? (float)_x1 : (float)_x2;
            float topY = (_y2 > _y1) ? (float)_y1 : (float)_y2;
            float width = Math.Abs((float)_x2 - (float)_x1);
            float height = Math.Abs((float)_y2 - (float)_y1);
            _allPoint = new List<Point>();
            _allPoint.Add(new Point(topX, topY + height * Constant.SECOND_QUARTER));
            _allPoint.Add(new Point(topX + width * Constant.FIRST_QUARTER, topY));
            _allPoint.Add(new Point(topX + width * Constant.THIRD_QUARTER, topY));
            _allPoint.Add(new Point(topX + width, topY + height * Constant.SECOND_QUARTER));
            _allPoint.Add(new Point(topX + width * Constant.THIRD_QUARTER, topY + height));
            _allPoint.Add(new Point(topX + width * Constant.FIRST_QUARTER, topY + height));
        }

        // Clone Shape
        public override Shape Clone()
        {
            return new Hexagon(this);
        }
    }
}
