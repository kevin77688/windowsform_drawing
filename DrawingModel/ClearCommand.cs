﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class ClearCommand : ICommand
    {
        Model _model;
        public ClearCommand(Model model)
        {
            _model = model;
        }

        // Clear command Execute
        public void Execute()
        {
            _model.ClearAllShape();
            _model.SelectedShape = null;
        }

        // Clear command Discard
        public void Discard()
        {
            _model.DiscardClearCommand();
        }
    }
}
