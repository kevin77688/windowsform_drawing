﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrawingModel
{
    public class CommandManager
    {
        Stack<ICommand> _undo = new Stack<ICommand>();
        Stack<ICommand> _redo = new Stack<ICommand>();

        // Execute command
        public void Execute(ICommand command)
        {
            command.Execute();
            // push command 進 undo stack
            _undo.Push(command);
            // 清除redo stack
            _redo.Clear();
        }

        // Undo command
        public void Undo()
        {
            if (_undo.Count <= 0)
                throw new Exception(Constant.UNDO_ERROR);
            ICommand command = _undo.Pop();
            _redo.Push(command);
            command.Discard();
        }

        // Redo command
        public void Redo()
        {
            if (_redo.Count <= 0)
                throw new Exception(Constant.REDO_ERROR);
            ICommand command = _redo.Pop();
            _undo.Push(command);
            command.Execute();
        }

        // Chech redo button status
        public bool IsRedoEnabled
        {
            get
            {
                return _redo.Count != 0;
            }
        }

        // Check undo button status
        public bool IsUndoEnabled
        {
            get
            {
                return _undo.Count != 0;
            }
        }
    }
}
