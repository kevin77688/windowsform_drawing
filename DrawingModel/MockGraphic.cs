﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class MockGraphic : IGraphics
    {
        List<Point> _pointList;
        string _drawString;

        // Clear all graphic
        public void ClearAll()
        {

        }

        // Get point list
        public List<Point> GetPointList()
        {
            return _pointList;
        }

        // Set point function
        private void SetPoint(List<Point> pointList)
        {
            //new List<Shape>(_shapes.ToArray())
            _pointList = new List<Point>(pointList.ToArray());
        }

        // Action while use DrawLine
        public void DrawLine(List<Point> pointList, bool highlight)
        {
            SetPoint(pointList);
        }

        // Action while use Draw Rectangle
        public void DrawRectangle(List<Point> pointList, bool highlight)
        {
            SetPoint(pointList);
        }

        // Action while use Draw Hexagon
        public void DrawHexagon(List<Point> pointList, bool highlight)
        {
            SetPoint(pointList);
        }

        // mock draw string
        public void DrawString(string text)
        {
            _drawString = text;
        }

        // get draw string
        public string GetDrawString()
        {
            return _drawString;
        }
    }
}
