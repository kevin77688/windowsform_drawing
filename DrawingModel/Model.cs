﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Model
    {
        public event ModelChangedEventHandler _modelChanged;
        public delegate void ModelChangedEventHandler();
        CommandManager _commandManager = new CommandManager();
        bool _isPressed = false;
        List<Shape> _shapes = new List<Shape>();
        Stack<List<Shape>> _clearShapes = new Stack<List<Shape>>();
        CloudManage cloud = new CloudManage();

        public Model()
        {
            CurrentState = new SelectState(this);
        }

        public IState CurrentState
        {
            get; set;
        }

        public Shape SelectedShape
        {
            get;set;
        }

        public List<Shape> GetShapeList()
        {
            return _shapes;
        }

        public void SetState(string currentShape)
        {
            if (currentShape == Constant.SELECTION)
                CurrentState = new SelectState(this);
            else
            {
                if (currentShape == Constant.LINE)
                    CurrentState = new DrawingState(this, new Line());
                else if (currentShape == Constant.RECTANGLE)
                    CurrentState = new DrawingState(this, new Rectangle());
                else if (currentShape == Constant.HEXAGON)
                    CurrentState = new DrawingState(this, new Hexagon());
            }
        }

        // Detect while mouse pressed 
        public void CheckPointerPressed(double pointX, double pointY)
        {
            _isPressed = true;
            CurrentState.SetFirstPoint(pointX, pointY);
        }

        // Detect while mouse move
        public void CheckPointerMoved(double pointX, double pointY)
        {
            if (_isPressed)
            {
                CurrentState.SetSecondPoint(pointX, pointY);
                NotifyModelChanged();
            }
        }

        // Detect while mouse release
        public void CheckPointerReleased()
        {
            if (_isPressed)
            {
                _isPressed = false;
                CurrentState.Execute();
                NotifyModelChanged();
            }
        }

        public void Execute(Shape shape)
        {
            _commandManager.Execute(new DrawCommand(this, shape));
            CurrentState = new SelectState(this);
        }

        // Clear all
        public void Clear()
        {
            _isPressed = false;
            _commandManager.Execute(new ClearCommand(this));
            NotifyModelChanged();
        }

        public void ResizeShape(List<Point> pointList)
        {
            _commandManager.Execute(new ResizeCommand(this, SelectedShape, pointList));
        }

        // Draw all shapes
        public void DrawAllShape(IGraphics graphics)
        {
            graphics.ClearAll();
            foreach (Shape shape in _shapes)
                shape.Draw(graphics);
            if (CurrentState.GetCreatedShape() != null)
                CurrentState.GetCreatedShape().Draw(graphics);
            if (SelectedShape != null)
            {
                Shape shape = SelectedShape.Clone();
                shape._highlight = true;
                shape.Draw(graphics);
            }
        }

        // Undo button action
        public void UndoButton()
        {
            _commandManager.Undo();
        }

        // Redo button action
        public void RedoButton()
        {
            _commandManager.Redo();
        }

        public bool IsRedoEnabled
        {
            get
            {
                return _commandManager.IsRedoEnabled;
            }
        }

        public bool IsUndoEnabled
        {
            get
            {
                return _commandManager.IsUndoEnabled;
            }
        }

        public bool IsClearButtonEnable
        {
            get
            {
                return _shapes.Count > 0;
            }
        }

        // Draw command
        public void DrawShape(Shape shape)
        {
            _shapes.Add(shape);
        }

        // Discard command
        public void DeleteShape()
        {
            if (_shapes.LastOrDefault() == SelectedShape)
                SelectedShape = null;
            _shapes.RemoveAt(_shapes.Count - 1);
        }

        // Clear command
        public void ClearAllShape()
        {
            _clearShapes.Push(new List<Shape>(_shapes.ToArray()));
            _shapes.Clear();
        }

        // Undo Clear Command
        public void DiscardClearCommand()
        {
            _shapes = _clearShapes.Pop();
        }

        public void SaveFile()
        {
            cloud.SaveFile(_commandManager, _shapes, _clearShapes);
        }

        // Notify changed
        void NotifyModelChanged()
        {
            if (_modelChanged != null)
                _modelChanged();
        }
    }
}
