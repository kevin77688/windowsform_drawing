﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class DrawingState : IState
    {
        public DrawingState (Model model, Shape shape)
        {
            _model = model;
            _currentShape = shape;
            _model.SelectedShape = null;
        }

        public override void SetFirstPoint(double pointX, double pointY)
        {
            _currentShape.SetFirstPoint(pointX, pointY);
        }

        public override void SetSecondPoint(double pointX, double pointY)
        {
            _currentShape.SetSecondPoint(pointX, pointY);
        }

        public override void Execute()
        {
            _model.Execute(_currentShape);
            _currentShape = null;
        }
        
        public override Shape GetCreatedShape()
        {
            return _currentShape;
        }
    }
}
