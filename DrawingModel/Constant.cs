﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Constant
    {
        public const string COLON = ", ";
        public const string END = ")";
        public const string LINE = "Line";
        public const string LINE_OUTPUT = "Line : (";
        public const string RECTANGLE = "Rectangle";
        public const string RECTANGLE_OUTPUT = "Rectangle : (";
        public const string HEXAGON = "Hexagon";
        public const string HEXAGON_OUTPUT = "Hexagon : (";
        public const string SELECTION = "Selection";
        public const string UNDO_ERROR = "Cannot Undo exception\n";
        public const string REDO_ERROR = "Cannot Redo exception\n";
        public const string DASH_ARRAY = "4 1 1 1 1 1";
        public const string FONT = "Arial";
        public const System.Int32 FONT_SIZE = 16;
        public const float FIRST_QUARTER = 0.25f;
        public const float SECOND_QUARTER = 0.5f;
        public const float THIRD_QUARTER = 0.75f;
        public const double SLOPE_ERROR = 0.1;
        public const float MARGIN_X = 5f;
        public const float MARGIN_Y = 85f;
        public const int NUMBER_ZERO = 0;
        public const int NUMBER_ONE = 1;
        public const int NUMBER_TWO = 2;
        public const int NUMBER_THREE = 3;
        public const int NUMBER_FOUR = 4;
        public const int NUMBER_FIVE = 5;
        public const int NUMBER_SIX = 6;
        public const int NUMBER_SEVEN = 7;
        public const int NUMBER_EIGHT = 8;
        public const int NUMBER_NINE = 9;
        public const int NUMBER_TEN = 10;
    }
}
