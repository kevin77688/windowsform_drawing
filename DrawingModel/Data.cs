﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DrawingModel
{
    [XmlRootAttribute("Data", Namespace = "DrawingModel", IsNullable = false)]
    public class Data
    {
        public void SetData(CommandManager commandManager, List<Shape> shapes, Stack<List<Shape>> clearShape)
        {
            _commandManager = commandManager;
            _shapes = shapes;
            _clearShape = clearShape;
        }

        public CommandManager _commandManager
        {
            get;set;
        }

        public List<Shape> _shapes
        {
            get;set;
        }

        public Stack<List<Shape>> _clearShape
        {
            get;set;
        }
    }
}
