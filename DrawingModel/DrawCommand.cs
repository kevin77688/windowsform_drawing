﻿using System;
//using System.Drawing;

namespace DrawingModel
{
    public class DrawCommand : ICommand
    {
        Shape _shape;
        Model _model;
        public DrawCommand(Model model, Shape shape)
        {
            _shape = shape;
            _model = model;
        }

        // Execute command
        public void Execute()
        {
            _model.DrawShape(_shape);
            _model.SelectedShape = null;
        }

        // Discard command
        public void Discard()
        {
            _model.DeleteShape();
        }
    }
}
