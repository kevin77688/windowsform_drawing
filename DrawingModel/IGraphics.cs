﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public interface IGraphics
    {
        // Clear all interface
        void ClearAll();

        // DrawLine Interface
        void DrawLine(List<Point> pointList, bool highlight);

        // Draw rectangle interface
        void DrawRectangle(List<Point> pointList, bool highlight);

        // Draw hexagon interface
        void DrawHexagon(List<Point> pointList, bool highlight);

        // Draw string interface
        void DrawString(string text);
    }
}
