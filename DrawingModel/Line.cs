﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Line : Shape
    {
        public Line()
        {

        }

        public Line(Line line)
        {
            _x1 = line._x1;
            _x2 = line._x2;
            _y1 = line._y1;
            _y2 = line._y2;
            _highlight = line._highlight;
        }

        // Draw line abstract class
        public override void Draw(IGraphics graphics)
        {
            
            List<Point> allPoint;
            allPoint = new List<Point>();
            allPoint.Add(new Point(_x1, _y1));
            allPoint.Add(new Point(_x2, _y2));
            graphics.DrawLine(allPoint, _highlight);
            graphics.DrawString(String.Empty);
            if (_highlight)
            {
                CheckPoint();
                graphics.DrawString(Constant.LINE_OUTPUT + _x1 + Constant.COLON + _y1 + Constant.COLON + _x2 + Constant.COLON + _y2 + Constant.END);
            }
        }

        // check point instde shape
        public override bool ClickCheck(double x1, double y1)
        {
            float topX = (this._x2 > this._x1) ? (float)this._x1 : (float)this._x2;
            float bottomX = (this._x2 > this._x1) ? (float)this._x2 : (float)this._x1;
            float topY = (this._y2 > this._y1) ? (float)this._y1 : (float)this._y2;
            float bottomY = (this._y2 > this._y1) ? (float)this._y2 : (float)this._y1;
            double slopeMouse = (x1 - _x1) / (y1 - _y1);
            double slopeOriginal = (_x1 - _x2) / (_y1 - _y2);
            if (Math.Abs(slopeMouse - slopeOriginal) < Constant.SLOPE_ERROR && x1 > topX && x1 < bottomX && y1 > topY && y1 < bottomY)
            {
                return true;
            }
            return false;
        }

        // Clone Shape
        public override Shape Clone()
        {
            return new Line(this);
        }
    }
}
