﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    class ResizeCommand : ICommand
    {
        Shape _shape;
        Model _model;
        List<Point> _pointList;
        public ResizeCommand(Model model, Shape shape, List<Point> pointList)
        {
            _model = model;
            _shape = shape;
            _pointList = pointList;
        }

        public void Execute()
        {
            _shape.SetSecondPoint(_pointList[1].pointX, _pointList[1].pointY);
        }

        public void Discard()
        {
            _shape.SetSecondPoint(_pointList[0].pointX, _pointList[0].pointY);
        }
    }
}
