﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class SelectState : IState
    {
        bool _resize = false;
        List<Point> _lastPointList = new List<Point>();
        Shape _currentSelectedShape;
        public SelectState(Model model)
        {
            _model = model;
            _lastPointList = new List<Point>
            {
                new Point(0, 0),
                new Point(0, 0)
            };
        }

        public override void SetFirstPoint(double pointX, double pointY)
        {
            if (_model.SelectedShape != null && _model.SelectedShape.IsInDistance(pointX, pointY))
            {
                _resize = true;
                _currentSelectedShape = _model.SelectedShape;
                _lastPointList[0] = _currentSelectedShape.GetSecondPoint();
                return;
            }
            List<Shape> _modelShapeList = _model.GetShapeList();
            for (int index = _modelShapeList.Count - 1; index >= 0; index--)
                if (_modelShapeList[index].ClickCheck(pointX, pointY))
                {
                    _model.SelectedShape = _modelShapeList[index];
                    return;
                }
            _model.SelectedShape = null;
        }

        public override void SetSecondPoint(double pointX, double pointY)
        {
            if (_resize)
            {
                if (pointX > _model.SelectedShape._x1)
                    _currentSelectedShape._x2 = pointX;
                if (pointY > _model.SelectedShape._y1)
                    _currentSelectedShape._y2 = pointY;
            }

        }

        public override void Execute()
        {
            if (_resize)
            {
                _lastPointList[1] = _currentSelectedShape.GetSecondPoint();
                _model.ResizeShape(_lastPointList);
                _resize = false;
            }
        }

        public override Shape GetCreatedShape()
        {
            return null;
        }
    }
}
