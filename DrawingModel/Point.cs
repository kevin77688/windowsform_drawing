﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Point
    {
        public Point(double firstX, double firstY)
        {
            pointX = firstX;
            pointY = firstY;
        }

        // Find Point x
        public double pointX
        {
            get;set;
        }

        // Find point Y
        public double pointY
        {
            get;set;
        }
    }
}
