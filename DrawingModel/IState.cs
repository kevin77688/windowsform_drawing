﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public abstract class IState
    {
        protected Model _model;
        public Shape _currentShape
        {
            get;set;
        }

        public abstract void SetFirstPoint(double pointX, double pointY);

        public abstract void SetSecondPoint(double pointX, double pointY);

        public abstract void Execute();

        public abstract Shape GetCreatedShape();
    }
}
