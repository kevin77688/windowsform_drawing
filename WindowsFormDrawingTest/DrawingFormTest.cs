﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace WindowsFormDrawingTest
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class DrawingFormTest
    {
        private const string FILE_PATH = @"..\\..\\..\\DrawingForm\\bin\\Debug\\DrawingForm.exe";
        private const string CALCULATOR_TITLE = "DrawingForm";
        public DrawingFormTest()
        {

        }

        // Test Initialize
        [TestInitialize()]
        public void Initialize()
        {
            Robot.Initialize(FILE_PATH, CALCULATOR_TITLE);
        }

        // Test Cleanup
        [TestCleanup()]
        public void Cleanup()
        {
            Robot.CleanUp();
        }

        // Test DrawingLine
        [TestMethod]
        public void DrawingLine()
        {
            DrawingTestTool.DrawLine("Panel", 200, 200, 400, 400);
            DrawingTestTool.ClickPanel("Panel", 300, 300);
        }

        // Test DrawingRectangle
        [TestMethod]
        public void DrawingRectangle()
        {
            DrawingTestTool.DrawRectangle("Panel", 200, 200, 400, 400);
            DrawingTestTool.ClickPanel("Panel", 300, 300);
        }

        // Test DrawingHexagon
        [TestMethod]
        public void DrawHexagon()
        {
            DrawingTestTool.DrawHexagon("Panel", 200, 200, 400, 400);
            DrawingTestTool.ClickPanel("Panel", 300, 300);
        }

        [TestMethod]
        public void DrawAllShape()
        {
            DrawingTestTool.DrawRectangle("Panel", 200, 200, 400, 400);
            DrawingTestTool.DrawHexagon("Panel", 200, 200, 400, 400);
            DrawingTestTool.DrawLine("Panel", 250, 200, 250, 400);
            DrawingTestTool.DrawLine("Panel", 350, 200, 350, 400);

        }
    }
}
