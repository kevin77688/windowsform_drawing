﻿namespace DrawingForm
{
    partial class DrawingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DrawingForm));
            this._backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this._canvas = new System.Windows.Forms.Panel();
            this._hexagonButton = new System.Windows.Forms.Button();
            this._rectangleButton = new System.Windows.Forms.Button();
            this._lineButton = new System.Windows.Forms.Button();
            this._clearButton = new System.Windows.Forms.Button();
            this._toolStrip1 = new System.Windows.Forms.ToolStrip();
            this._undoButton = new System.Windows.Forms.ToolStripButton();
            this._redoButton = new System.Windows.Forms.ToolStripButton();
            this._uploadButton = new System.Windows.Forms.ToolStripButton();
            this._downloadButton = new System.Windows.Forms.ToolStripButton();
            this._canvas.SuspendLayout();
            this._toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _canvas
            // 
            this._canvas.AccessibleName = "Panel";
            this._canvas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._canvas.BackColor = System.Drawing.Color.LightYellow;
            this._canvas.Controls.Add(this._hexagonButton);
            this._canvas.Controls.Add(this._rectangleButton);
            this._canvas.Controls.Add(this._lineButton);
            this._canvas.Controls.Add(this._clearButton);
            this._canvas.Location = new System.Drawing.Point(16, 33);
            this._canvas.Margin = new System.Windows.Forms.Padding(4);
            this._canvas.Name = "_canvas";
            this._canvas.Size = new System.Drawing.Size(1035, 550);
            this._canvas.TabIndex = 0;
            this._canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.HandleCanvasPaint);
            this._canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.HandleCanvasPressed);
            this._canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.HandleCanvasMoved);
            this._canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.HandleCanvasReleased);
            // 
            // _hexagonButton
            // 
            this._hexagonButton.AccessibleName = "Hexagon";
            this._hexagonButton.Font = new System.Drawing.Font("PMingLiU", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._hexagonButton.Location = new System.Drawing.Point(555, 20);
            this._hexagonButton.Margin = new System.Windows.Forms.Padding(20);
            this._hexagonButton.Name = "_hexagonButton";
            this._hexagonButton.Size = new System.Drawing.Size(200, 85);
            this._hexagonButton.TabIndex = 3;
            this._hexagonButton.Text = "Hexagon";
            this._hexagonButton.UseVisualStyleBackColor = true;
            this._hexagonButton.Click += new System.EventHandler(this.ChangeShape);
            // 
            // _rectangleButton
            // 
            this._rectangleButton.AccessibleName = "Rectangle";
            this._rectangleButton.Font = new System.Drawing.Font("PMingLiU", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._rectangleButton.Location = new System.Drawing.Point(296, 20);
            this._rectangleButton.Margin = new System.Windows.Forms.Padding(20);
            this._rectangleButton.Name = "_rectangleButton";
            this._rectangleButton.Size = new System.Drawing.Size(200, 85);
            this._rectangleButton.TabIndex = 2;
            this._rectangleButton.Text = "Rectangle";
            this._rectangleButton.UseVisualStyleBackColor = true;
            this._rectangleButton.Click += new System.EventHandler(this.ChangeShape);
            // 
            // _lineButton
            // 
            this._lineButton.AccessibleName = "Line";
            this._lineButton.Font = new System.Drawing.Font("PMingLiU", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._lineButton.Location = new System.Drawing.Point(20, 20);
            this._lineButton.Margin = new System.Windows.Forms.Padding(20);
            this._lineButton.Name = "_lineButton";
            this._lineButton.Size = new System.Drawing.Size(200, 85);
            this._lineButton.TabIndex = 1;
            this._lineButton.Text = "Line";
            this._lineButton.UseVisualStyleBackColor = true;
            this._lineButton.Click += new System.EventHandler(this.ChangeShape);
            // 
            // _clearButton
            // 
            this._clearButton.AccessibleName = "Clear";
            this._clearButton.Enabled = false;
            this._clearButton.Font = new System.Drawing.Font("PMingLiU", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._clearButton.Location = new System.Drawing.Point(811, 20);
            this._clearButton.Margin = new System.Windows.Forms.Padding(4);
            this._clearButton.Name = "_clearButton";
            this._clearButton.Size = new System.Drawing.Size(200, 85);
            this._clearButton.TabIndex = 0;
            this._clearButton.Text = "Clear";
            this._clearButton.UseVisualStyleBackColor = true;
            this._clearButton.Click += new System.EventHandler(this.HandleClearButtonClick);
            // 
            // _toolStrip1
            // 
            this._toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this._toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._undoButton,
            this._redoButton,
            this._uploadButton,
            this._downloadButton});
            this._toolStrip1.Location = new System.Drawing.Point(0, 0);
            this._toolStrip1.Name = "_toolStrip1";
            this._toolStrip1.Size = new System.Drawing.Size(1067, 27);
            this._toolStrip1.TabIndex = 1;
            this._toolStrip1.Text = "toolStrip1";
            // 
            // _undoButton
            // 
            this._undoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._undoButton.Enabled = false;
            this._undoButton.Image = ((System.Drawing.Image)(resources.GetObject("_undoButton.Image")));
            this._undoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._undoButton.Name = "_undoButton";
            this._undoButton.Size = new System.Drawing.Size(49, 24);
            this._undoButton.Text = "Undo";
            this._undoButton.Click += new System.EventHandler(this.UndoHandle);
            // 
            // _redoButton
            // 
            this._redoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._redoButton.Enabled = false;
            this._redoButton.Image = ((System.Drawing.Image)(resources.GetObject("_redoButton.Image")));
            this._redoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._redoButton.Name = "_redoButton";
            this._redoButton.Size = new System.Drawing.Size(48, 24);
            this._redoButton.Text = "Redo";
            this._redoButton.Click += new System.EventHandler(this.RedoHandle);
            // 
            // _uploadButton
            // 
            this._uploadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._uploadButton.Image = ((System.Drawing.Image)(resources.GetObject("_uploadButton.Image")));
            this._uploadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._uploadButton.Name = "_uploadButton";
            this._uploadButton.Size = new System.Drawing.Size(29, 24);
            this._uploadButton.Text = "Upload";
            this._uploadButton.Click += new System.EventHandler(this.Upload);
            // 
            // _downloadButton
            // 
            this._downloadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._downloadButton.Enabled = false;
            this._downloadButton.Image = ((System.Drawing.Image)(resources.GetObject("_downloadButton.Image")));
            this._downloadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._downloadButton.Name = "_downloadButton";
            this._downloadButton.Size = new System.Drawing.Size(29, 24);
            this._downloadButton.Text = "Download";
            // 
            // DrawingForm
            // 
            this.AccessibleName = "DrawingForm";
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 599);
            this.Controls.Add(this._toolStrip1);
            this.Controls.Add(this._canvas);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "DrawingForm";
            this.Text = "DrawingForm";
            this._canvas.ResumeLayout(false);
            this._toolStrip1.ResumeLayout(false);
            this._toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker _backgroundWorker1;
        private System.Windows.Forms.Button _clearButton;
        private System.Windows.Forms.Button _lineButton;
        private System.Windows.Forms.Button _rectangleButton;
        private System.Windows.Forms.Panel _canvas;
        private System.Windows.Forms.Button _hexagonButton;
        private System.Windows.Forms.ToolStrip _toolStrip1;
        private System.Windows.Forms.ToolStripButton _undoButton;
        private System.Windows.Forms.ToolStripButton _redoButton;
        private System.Windows.Forms.ToolStripButton _uploadButton;
        private System.Windows.Forms.ToolStripButton _downloadButton;
    }
}

