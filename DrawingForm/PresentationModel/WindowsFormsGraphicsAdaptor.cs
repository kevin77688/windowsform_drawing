﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using DrawingModel;

namespace DrawingForm.PresentationModel
{
    public class WindowsFormsGraphicsAdaptor : IGraphics
    {
        Graphics _graphics;
        public WindowsFormsGraphicsAdaptor(Graphics graphics)
        {
            this._graphics = graphics;
        }

        // Action while clear all
        public void ClearAll()
        {
            // OnPaint時會自動清除畫面，因此不需實作
        }

        // Action while use DrawLine
        public void DrawLine(List<DrawingModel.Point> pointList, bool highlight)
        {
            _graphics.DrawLine(Pens.Black, (float)pointList[0].pointX, (float)pointList[0].pointY, (float)pointList[1].pointX, (float)pointList[1].pointY);
            if (highlight)
            {
                float[] dashValues = { Constant.NUMBER_EIGHT, Constant.NUMBER_FOUR };
                Pen bluePen = new Pen(Color.Blue, Constant.NUMBER_THREE);
                bluePen.DashPattern = dashValues;
                _graphics.DrawLine(bluePen, (float)pointList[0].pointX, (float)pointList[0].pointY, (float)pointList[1].pointX, (float)pointList[1].pointY);
                DrawDot(new PointF((float)pointList[0].pointX, (float)pointList[0].pointY));
                DrawDot(new PointF((float)pointList[1].pointX, (float)pointList[1].pointY));
            }
        }

        // Action while use Draw Rectangle
        public void DrawRectangle(List<DrawingModel.Point> pointList, bool highlight)
        {
            DrawPolygon(Constant.NUMBER_FOUR, pointList, Color.Red, highlight);
        }

        // Action while use Draw Hexagon
        public void DrawHexagon(List<DrawingModel.Point> pointList, bool highlight)
        {
            DrawPolygon(Constant.NUMBER_SIX, pointList, Color.LightBlue, highlight);
        }

        // Action while Draw polygon
        public void DrawPolygon(int dimension, List<DrawingModel.Point> pointList, Color color, bool highlight)
        {
            PointF[] shape = new PointF[dimension];
            for (int index = 0; index < dimension; index++)
                shape[index] = new PointF((float)pointList[index].pointX, (float)pointList[index].pointY);
            if (highlight)
                HighlightShape(pointList);
            else
                _graphics.FillPolygon(new SolidBrush(color), shape);
        }

        // Draw string on panel
        public void DrawString(string text)
        {
            Font style = new Font(Constant.FONT, Constant.FONT_SIZE);
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            float marginX = Constant.MARGIN_X;
            float marginY = Constant.MARGIN_Y;
            StringFormat drawFormat = new StringFormat();
            _graphics.DrawString(text, style, drawBrush, marginX, marginY, drawFormat);
        }

        // Draw while highlight shape
        private void HighlightShape(List<DrawingModel.Point> pointList)
        {
            float[] dashValues = { Constant.NUMBER_FOUR, Constant.NUMBER_TWO };
            Pen blackPen = new Pen(Color.Black, Constant.NUMBER_TWO);
            blackPen.DashPattern = dashValues;
            PointF[] shape = new PointF[Constant.NUMBER_FOUR];
            DrawingModel.Point maximumPoint = FindMaximum(pointList);
            DrawingModel.Point minimumPoint = FindMinimum(pointList);
            shape[Constant.NUMBER_ZERO] = new PointF((float)minimumPoint.pointX, (float)minimumPoint.pointY);
            shape[Constant.NUMBER_ONE] = new PointF((float)maximumPoint.pointX, (float)minimumPoint.pointY);
            shape[Constant.NUMBER_TWO] = new PointF((float)maximumPoint.pointX, (float)maximumPoint.pointY);
            shape[Constant.NUMBER_THREE] = new PointF((float)minimumPoint.pointX, (float)maximumPoint.pointY);
            _graphics.DrawPolygon(blackPen, shape);
            foreach (PointF point in shape)
                DrawDot(point);
        }

        // draw white dot
        private void DrawDot(PointF point)
        {
            _graphics.DrawEllipse(new Pen(Color.Black), (float)point.X - Constant.NUMBER_FIVE, (float)point.Y - Constant.NUMBER_FIVE, Constant.NUMBER_TEN, Constant.NUMBER_TEN);
            _graphics.FillEllipse(new SolidBrush(Color.White), (float)point.X - Constant.NUMBER_FIVE, (float)point.Y - Constant.NUMBER_FIVE, Constant.NUMBER_TEN, Constant.NUMBER_TEN);
        }

        // Find maximum dot
        private DrawingModel.Point FindMaximum(List<DrawingModel.Point> pointList)
        {
            float pointX = 0;
            float pointY = 0;
            foreach (DrawingModel.Point point in pointList)
            {
                if (point.pointX > pointX)
                    pointX = (float)point.pointX;
                if (point.pointY > pointY)
                    pointY = (float)point.pointY;
            }
            return new DrawingModel.Point(pointX, pointY);
        }

        // Find mimimum dot
        private DrawingModel.Point FindMinimum(List<DrawingModel.Point> pointList)
        {
            float pointX = (float)pointList[0].pointX;
            float pointY = (float)pointList[0].pointY;
            foreach (DrawingModel.Point point in pointList)
            {
                if (point.pointX < pointX)
                    pointX = (float)point.pointX;
                if (point.pointY < pointY)
                    pointY = (float)point.pointY;
            }
            return new DrawingModel.Point(pointX, pointY);
        }
    }
}