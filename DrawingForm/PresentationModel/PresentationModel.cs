﻿using DrawingModel;
using System.Windows.Forms;
using System;

namespace DrawingForm.PresentationModel
{
    public class PresentationModel
    {
        Model _model;
        public PresentationModel(Model model, Control canvas)
        {
            this._model = model;
        }

        // Draw presentation model
        public void Draw(System.Drawing.Graphics graphics)
        {
            // graphics物件是Paint事件帶進來的，只能在當次Paint使用
            // 而Adaptor又直接使用graphics，這樣DoubleBuffer才能正確運作
            // 因此，Adaptor不能重複使用，每次都要重新new
            _model.DrawAllShape(new WindowsFormsGraphicsAdaptor(graphics));
        }

        // True to False, False to True
        public bool ChangeStatus(bool set)
        {
            return !set;
        }
    }
}