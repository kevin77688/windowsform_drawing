﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawingForm
{
    public partial class DrawingForm : Form
    {
        DrawingModel.Model _model;
        PresentationModel.PresentationModel _presentationModel;
        List<Button> _allShapeButton = new List<Button>();
        public DrawingForm()
        {
            InitializeComponent();
            _model = new DrawingModel.Model();
            _presentationModel = new PresentationModel.PresentationModel(_model, _canvas);
            _model._modelChanged += HandleModelChanged;
            _allShapeButton.Add(_lineButton);
            _allShapeButton.Add(_rectangleButton);
            _allShapeButton.Add(_hexagonButton);
        }

        // While click clear button
        public void HandleClearButtonClick(object sender, EventArgs e)
        {
            _model.Clear();
        }

        // While canvas pressed
        public void HandleCanvasPressed(object sender, MouseEventArgs e)
        {
            _model.CheckPointerPressed(e.X, e.Y);
        }

        // While move mouse
        public void HandleCanvasMoved(object sender, MouseEventArgs e)
        {
            _model.CheckPointerMoved(e.X, e.Y);
        }

        // While release mouse
        public void HandleCanvasReleased(object sender, MouseEventArgs e)
        {
            _model.CheckPointerReleased();
            foreach (Button button in _allShapeButton)
                button.Enabled = true;
            _model.SetState(DrawingModel.Constant.SELECTION);
        }

        // Paint the canvas
        public void HandleCanvasPaint(object sender, PaintEventArgs e)
        {
            _presentationModel.Draw(e.Graphics);
        }

        // Undo button action
        public void UndoHandle(object sender, EventArgs e)
        {
            _model.UndoButton();
            HandleModelChanged();
        }

        // Redo button action
        public void RedoHandle(object sender, EventArgs e)
        {
            _model.RedoButton();
            HandleModelChanged();
        }

        // Check Model Changed
        public void HandleModelChanged()
        {
            _redoButton.Enabled = _model.IsRedoEnabled;
            _undoButton.Enabled = _model.IsUndoEnabled;
            _clearButton.Enabled = _model.IsClearButtonEnable;
            Invalidate(true);
        }

        // Change button action
        private void ChangeShape(object sender, EventArgs e)
        {
            Button currentButton = (Button)sender;
            _model.SetState(currentButton.Text);
            foreach (Button button in _allShapeButton)
                button.Enabled = true;
            currentButton.Enabled = false;
        }

        private void Upload(object sender, EventArgs e)
        {
            _model.SaveFile();
        }
    }
}
