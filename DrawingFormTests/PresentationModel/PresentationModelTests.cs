﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingForm.PresentationModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawingModel;
using System.Windows.Forms;
using System.Drawing;

namespace DrawingForm.PresentationModel.Tests
{
    [TestClass()]
    public class PresentationModelTests
    {
        Model _model;
        PresentationModel _presentationModel;
        DrawingForm _form;
        PrivateObject _privateObject;

        // Test Initialize
        [TestInitialize()]
        public void PresentationModelTest()
        {
            _model = new Model();
            _form = new DrawingForm();
            Panel panel = new Panel();
            _presentationModel = new PresentationModel(_model, panel);
            _privateObject = new PrivateObject(_model);
        }

        // Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        // Test ChangeStatus
        [TestMethod()]
        public void ChangeStatusTest()
        {
            Assert.IsFalse(_presentationModel.ChangeStatus(true));
            Assert.IsTrue(_presentationModel.ChangeStatus(false));
        }

        // Test Draw
        [TestMethod()]
        public void DrawTest()
        {
            _model.SetState("Line");
            _model.CheckPointerPressed(0, 1);
            _model.CheckPointerMoved(2, 3);
            _model.CheckPointerReleased();
            Panel p = new Panel();
            Graphics g = p.CreateGraphics();
            _presentationModel.Draw(g);
            Assert.AreEqual(1, ((List<Shape>)_privateObject.GetField("_shapes")).Count);
        }
    }
}