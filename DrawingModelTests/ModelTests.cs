﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class ModelTests
    {
        Model _model;
        PrivateObject _privateModel;
        MockGraphic _mockGraphic;
        // Test Initialize
        [TestInitialize()]
        public void Initialize()
        {
            _model = new Model();
            _privateModel = new PrivateObject(_model);
            _mockGraphic = new MockGraphic();
        }

        // Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        // Test
        [TestMethod()]
        public void SetStateTest()
        {
            _model.SetState("Line");
            Assert.AreEqual("Line", _privateModel.GetField("_currentState"));
            _model.SetState("Selection");
            Assert.AreEqual("Selection", _privateModel.GetField("_currentState"));
        }

        // Test
        [TestMethod()]
        public void CheckPointerPressedTest()
        {
            _model.SetState("Line");
            _model.CheckPointerPressed(1, 2);
            Assert.IsFalse((bool)_privateModel.GetField("_selectionMode"));
            Assert.AreEqual(typeof(Line), _privateModel.GetField("_shapeHint").GetType());
            Assert.IsTrue((bool)_privateModel.GetField("_isPressed"));
        }

        // Test
        [TestMethod()]
        public void SetPointTest()
        {
            _model.SetState("Rectangle");
            _model.SetPoint(1, 2);
            Assert.AreEqual(typeof(Rectangle), _privateModel.GetField("_shapeHint").GetType());
            Assert.IsFalse((bool)_privateModel.GetField("_selectionMode"));
            _model.SetState("Selection");
            _model.SetPoint(5, 2);
            Assert.IsTrue((bool)_privateModel.GetField("_selectionMode"));
        }

        // Test
        [TestMethod()]
        public void SetPointWithCurrentShapeTest()
        {
            _model.SetState("Rectangle");
            _model.SetPoint(1, 1);
            _model.CheckPointerMoved(9, 9);
            _model.CheckPointerReleased();
            _model.SetState("Selection");
            _model.SetPoint(5, 5);
            _model.SetPoint(6, 6);
            PrivateObject p = new PrivateObject(_model);
            Shape s = (Shape)p.GetField("_currentShape");
            Assert.IsTrue(s == null);
        }

        // Test
        [TestMethod()]
        public void CheckClickShapeTest()
        {
            Rectangle rectangle = new Rectangle();
            rectangle.SetFirstPoint(1, 1);
            rectangle.SetSecondPoint(5, 5);
            rectangle.Draw(_mockGraphic);
            _model.DrawShape(rectangle);
            _model.CheckClickShape(4, 4);
            var currentList = _privateModel.GetField("_shapes");
            Assert.AreEqual(typeof(Rectangle), ((List<Shape>)currentList)[0].GetType());
        }

        // Test
        [TestMethod()]
        public void CheckPointerMovedTest()
        {
            _model.SetState("Rectangle");
            _model.CheckPointerPressed(1, 1);
            _model.CheckPointerMoved(10, 20);
            Shape s = (Shape)_privateModel.GetField("_shapeHint");
            Assert.AreEqual(10, s._x2);
            Assert.AreEqual(20, s._y2);
        }

        // Test
        [TestMethod()]
        public void CheckPointerReleasedTest()
        {
            _model.SetState("Rectangle");
            _model.CheckPointerPressed(1, 1);
            _model.CheckPointerMoved(10, 20);
            _model.CheckPointerReleased();
            _model.DrawAllShape(_mockGraphic);
            Assert.IsFalse((bool)_privateModel.GetField("_isPressed"));
            Assert.AreEqual(null, _privateModel.GetField("_shapeHint"));
            _model.SetState("Selection");
            _model.CheckPointerPressed(5, 5);
            Shape s = (Shape)_privateModel.GetField("_currentShape");
            Assert.IsFalse(s.highlight);
        }

        // Test
        [TestMethod()]
        public void ClearTest()
        {
            _model.Clear();
            Assert.IsFalse((bool)_privateModel.GetField("_isPressed"));
            Assert.IsTrue((bool)_privateModel.GetField("_selectionMode"));
        }

        // Test
        [TestMethod()]
        public void DrawAllShapeTest()
        {
            _model.SetState("Rectangle");
            _model.CheckPointerPressed(1, 1);
            _model.CheckPointerMoved(10, 20);
            _model.CheckPointerReleased();
            _model.SetState("Hexagon");
            _model.CheckPointerPressed(20, 50);
            _model.CheckPointerMoved(100, 200);
            _model.CheckPointerReleased();
            _model.DrawAllShape(_mockGraphic);
            List<Shape> s = (List<Shape>)_privateModel.GetField("_shapes");
            Assert.AreEqual(2, s.Count);
            _model.CheckPointerPressed(50, 50);
            _model.CheckPointerMoved(20, 30);
            Assert.IsTrue(((Shape)_privateModel.GetField("_shapeHint")) != null);
        }

        // Test
        [TestMethod()]
        public void DrawAllShapeHintTest()
        {
            _model.SetState("Line");
            _model.CheckPointerPressed(1, 2);
            _model.CheckPointerMoved(3, 4);
            _model.DrawAllShape(_mockGraphic);
            Shape s = (Shape)_privateModel.GetField("_shapeHint");
            Assert.IsTrue(s != null);
        }

        // Test
        [TestMethod()]
        public void UndoButtonTest()
        {
            _model.SetState("Rectangle");
            _model.CheckPointerPressed(1, 1);
            _model.CheckPointerMoved(10, 20);
            _model.CheckPointerReleased();
            _model.DrawAllShape(_mockGraphic);
            _model.UndoButton();
            List<Shape> s = (List<Shape>)_privateModel.GetField("_shapes");
            Assert.AreEqual(0, s.Count);
        }

        // Test
        [TestMethod()]
        public void UndoButtonTest2()
        {
            _model.SetState("Rectangle");
            _model.CheckPointerPressed(1, 1);
            _model.CheckPointerMoved(10, 20);
            _model.CheckPointerReleased();
            _model.SetState("Hexagon");
            _model.CheckPointerPressed(20, 50);
            _model.CheckPointerMoved(100, 200);
            _model.CheckPointerReleased();
            _model.DrawAllShape(_mockGraphic);
            _model.SetState("Selection");
            _model.CheckPointerPressed(5, 5);
            _model.CheckPointerReleased();
            _model.UndoButton();
            List<Shape> s = (List<Shape>)_privateModel.GetField("_shapes");
            Assert.AreEqual(1, s.Count);
        }

        // Test
        [TestMethod()]
        public void RedoButtonTest()
        {
            _model.SetState("Rectangle");
            _model.CheckPointerPressed(1, 1);
            _model.CheckPointerMoved(10, 20);
            _model.CheckPointerReleased();
            _model.SetState("Hexagon");
            _model.CheckPointerPressed(20, 50);
            _model.CheckPointerMoved(100, 200);
            _model.CheckPointerReleased();
            _model.DrawAllShape(_mockGraphic);
            _model.SetState("Selection");
            _model.CheckPointerPressed(5, 5);
            _model.CheckPointerReleased();
            _model.UndoButton();
            List<Shape> s = (List<Shape>)_privateModel.GetField("_shapes");
            Assert.AreEqual(1, s.Count);
            _model.RedoButton();
            Assert.AreEqual(2, s.Count);
        }

        // Test
        [TestMethod()]
        public void DrawShapeTest()
        {
            List<Shape> s = (List<Shape>)_privateModel.GetField("_shapes");
            Assert.AreEqual(0, s.Count);
            Shape _rectangle = new Rectangle();
            _rectangle.SetFirstPoint(1, 2);
            _rectangle.SetSecondPoint(3, 4);
            _model.DrawShape(_rectangle);
            Assert.AreEqual(1, s.Count);
        }

        // Test
        [TestMethod()]
        public void DeleteShapeTest()
        {
            List<Shape> s = (List<Shape>)_privateModel.GetField("_shapes");
            Shape _rectangle = new Rectangle();
            _rectangle.SetFirstPoint(1, 2);
            _rectangle.SetSecondPoint(3, 4);
            _model.DrawShape(_rectangle);
            _model.DrawShape(_rectangle);
            Assert.AreEqual(2, s.Count);
            _model.DeleteShape();
            Assert.AreEqual(1, s.Count);
        }

        // Test
        [TestMethod()]
        public void ClearAllShapeTest()
        {
            List<Shape> s = (List<Shape>)_privateModel.GetField("_shapes");
            Shape _rectangle = new Rectangle();
            _rectangle.SetFirstPoint(1, 2);
            _rectangle.SetSecondPoint(3, 4);
            _model.DrawShape(_rectangle);
            _model.DrawShape(_rectangle);
            Assert.AreEqual(2, s.Count);
            _model.ClearAllShape();
            Assert.AreEqual(0, s.Count);
        }

        // Test
        [TestMethod()]
        public void DiscardClearCommandTest()
        {
            List<Shape> s = (List<Shape>)_privateModel.GetField("_shapes");
            Shape _rectangle = new Rectangle();
            _rectangle.SetFirstPoint(1, 2);
            _rectangle.SetSecondPoint(3, 4);
            _model.DrawShape(_rectangle);
            _model.DrawShape(_rectangle);
            Assert.AreEqual(2, s.Count);
            _model.ClearAllShape();
            Assert.AreEqual(0, s.Count);
            _model.DiscardClearCommand();
            List<Shape> ss = (List<Shape>)_privateModel.GetField("_shapes");
            Assert.AreEqual(2, ss.Count);
        }

        // Test
        [TestMethod()]
        public void IsUndoEnableTest()
        {
            Assert.IsFalse(_model.IsUndoEnabled);
            _model.SetState("Rectangle");
            _model.CheckPointerPressed(1, 1);
            _model.CheckPointerMoved(10, 20);
            _model.CheckPointerReleased();
            Assert.IsTrue(_model.IsUndoEnabled);
        }

        // Test
        [TestMethod()]
        public void IsRedoEnableTest()
        {
            Assert.IsFalse(_model.IsRedoEnabled);
            _model.SetState("Rectangle");
            _model.CheckPointerPressed(1, 1);
            _model.CheckPointerMoved(10, 20);
            _model.CheckPointerReleased();
            Assert.IsFalse(_model.IsRedoEnabled);
            _model.UndoButton();
            Assert.IsTrue(_model.IsRedoEnabled);
        }

        // Test
        [TestMethod()]
        public void IsClearButtonEnableTest()
        {
            Assert.IsFalse(_model.IsClearButtonEnable);
            _model.SetState("Rectangle");
            _model.CheckPointerPressed(1, 1);
            _model.CheckPointerMoved(10, 20);
            _model.CheckPointerReleased();
            Assert.IsTrue(_model.IsClearButtonEnable);
            _model.Clear();
            Assert.IsFalse(_model.IsClearButtonEnable);
        }
    }
}