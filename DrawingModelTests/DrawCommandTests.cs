﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class DrawCommandTests
    {
        DrawCommand _drawCommand;
        PrivateObject _privateCommand;
        [TestInitialize()]
        public void Initialize()
        {
            Shape s = new Rectangle();
            s.SetFirstPoint(1, 1);
            s.SetSecondPoint(6, 6);
            _drawCommand = new DrawCommand(new Model(), s);
            _privateCommand = new PrivateObject(_drawCommand);
        }

        // Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        [TestMethod()]
        public void ExecuteTest()
        {
            _drawCommand.Execute();
            Assert.AreEqual(typeof(Rectangle), ((Shape)_privateCommand.GetField("_shape")).GetType());
        }

        [TestMethod()]
        public void DiscardTest()
        {
            _drawCommand.Execute();
            _drawCommand.Discard();
            Assert.AreEqual(typeof(Rectangle), ((Shape)_privateCommand.GetField("_shape")).GetType());
        }
    }
}