﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class RectangleTests
    {
        Rectangle _rectangle;

        // Test Initialize
        [TestInitialize()]
        public void Initialize()
        {
            _rectangle = new Rectangle();
        }

        // Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        // Test
        [TestMethod()]
        public void SetFirstPointTest()
        {
            _rectangle.SetFirstPoint(0, 1);
            Assert.AreEqual(0, _rectangle._x1);
            Assert.AreEqual(1, _rectangle._y1);
        }

        // Test
        [TestMethod()]
        public void SetSecondPointTest()
        {
            _rectangle.SetSecondPoint(2, 3);
            Assert.AreEqual(2, _rectangle._x2);
            Assert.AreEqual(3, _rectangle._y2);
        }

        // Test
        [TestMethod()]
        public void HighlightTest()
        {
            Assert.IsFalse(_rectangle.highlight);
            _rectangle.highlight = true;
            Assert.IsTrue(_rectangle.highlight);
        }

        // Test
        [TestMethod()]
        public void DrawTest()
        {
            var mockGraphic = new MockGraphic();
            _rectangle.SetFirstPoint(1, 2);
            _rectangle.SetSecondPoint(3, 4);
            _rectangle.Draw(mockGraphic);
            List<Point> pointList = mockGraphic.GetPointList();
            Assert.AreEqual(1, pointList[0].pointX);
            Assert.AreEqual(2, pointList[0].pointY);
            Assert.AreEqual(3, pointList[1].pointX);
            Assert.AreEqual(2, pointList[1].pointY);
            Assert.AreEqual(3, pointList[2].pointX);
            Assert.AreEqual(4, pointList[2].pointY);
            Assert.AreEqual(1, pointList[3].pointX);
            Assert.AreEqual(4, pointList[3].pointY);
        }

        // Test
        [TestMethod()]
        public void ClickCheckTest()
        {
            var mockGraphic = new MockGraphic();
            _rectangle.SetFirstPoint(1, 1);
            _rectangle.SetSecondPoint(3, 3);
            _rectangle.Draw(mockGraphic);
            Assert.IsTrue(_rectangle.ClickCheck(2, 2));
            Assert.IsFalse(_rectangle.ClickCheck(5, 5));
        }

        // Test 
        [TestMethod()]
        public void DrawHighlightTest()
        {
            var mockGraphic = new MockGraphic();
            _rectangle.SetFirstPoint(1, 2);
            _rectangle.SetSecondPoint(3, 4);
            _rectangle.highlight = true;
            _rectangle.Draw(mockGraphic);
            Assert.AreEqual("Rectangle : (1, 2, 3, 4)", mockGraphic.GetDrawString());
        }
    }
}