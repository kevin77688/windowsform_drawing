﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class ClearCommandTests
    {
        ClearCommand _clearCommand;
        Model _model;
        [TestInitialize()]
        public void ClearCommandTest()
        {
            _model = new Model();
            _clearCommand = new ClearCommand(_model);
        }

        [TestMethod()]
        public void ExecuteTest()
        {
            Shape s = new Rectangle();
            s.SetFirstPoint(1, 2);
            s.SetSecondPoint(5, 6);
            _model.DrawShape(s);
            _model.DrawShape(s);
            PrivateObject privateModel = new PrivateObject(_model);
            Assert.AreEqual(2, ((List<Shape>)privateModel.GetField("_shapes")).Count);
            _clearCommand.Execute();
            Assert.AreEqual(0, ((List<Shape>)privateModel.GetField("_shapes")).Count);
        }

        [TestMethod()]
        public void DiscardTest()
        {
            Shape s = new Rectangle();
            s.SetFirstPoint(1, 2);
            s.SetSecondPoint(5, 6);
            _model.DrawShape(s);
            _model.DrawShape(s);
            PrivateObject privateModel = new PrivateObject(_model);
            Assert.AreEqual(2, ((List<Shape>)privateModel.GetField("_shapes")).Count);
            _clearCommand.Execute();
            Assert.AreEqual(0, ((List<Shape>)privateModel.GetField("_shapes")).Count);
            _clearCommand.Discard();
            Assert.AreEqual(2, ((List<Shape>)privateModel.GetField("_shapes")).Count);
        }
    }
}