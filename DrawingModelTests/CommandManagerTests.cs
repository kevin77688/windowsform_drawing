﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class CommandManagerTests
    {
        CommandManager _commandManager;
        MockCommand _mockCommand;
        PrivateObject _privateCommand;
        [TestInitialize()]
        public void Initialize()
        {
            _mockCommand = new MockCommand();
            _commandManager = new CommandManager();
            _privateCommand = new PrivateObject(_commandManager);
        }

        // Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        [TestMethod()]
        public void ExecuteTest()
        {
            _commandManager.Execute(_mockCommand);
            Assert.AreEqual(1, ((Stack<ICommand>)_privateCommand.GetField("_undo")).Count);
            Assert.AreEqual(0, ((Stack<ICommand>)_privateCommand.GetField("_redo")).Count);
        }

        [TestMethod()]
        public void UndoTest()
        {
            _commandManager.Execute(_mockCommand);
            _commandManager.Undo();
            Assert.AreEqual(0, ((Stack<ICommand>)_privateCommand.GetField("_undo")).Count);
            Assert.AreEqual(1, ((Stack<ICommand>)_privateCommand.GetField("_redo")).Count);
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception), Constant.UNDO_ERROR)]
        public void UndoErrorTest()
        {
            _commandManager.Undo();
        }

        [TestMethod()]
        public void RedoTest()
        {
            _commandManager.Execute(_mockCommand);
            _commandManager.Undo();
            Assert.AreEqual(0, ((Stack<ICommand>)_privateCommand.GetField("_undo")).Count);
            Assert.AreEqual(1, ((Stack<ICommand>)_privateCommand.GetField("_redo")).Count);
            _commandManager.Redo();
            Assert.AreEqual(1, ((Stack<ICommand>)_privateCommand.GetField("_undo")).Count);
            Assert.AreEqual(0, ((Stack<ICommand>)_privateCommand.GetField("_redo")).Count);
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception), Constant.REDO_ERROR)]
        public void RedoErrorTest()
        {
            _commandManager.Redo();
        }

        [TestMethod()]
        public void IsUndoEnabledTest()
        {
            Assert.IsFalse(_commandManager.IsUndoEnabled);
            _commandManager.Execute(_mockCommand);
            Assert.IsTrue(_commandManager.IsUndoEnabled);
        }

        [TestMethod()]
        public void IsRedoEnableTest()
        {
            Assert.IsFalse(_commandManager.IsRedoEnabled);
            _commandManager.Execute(_mockCommand);
            Assert.IsFalse(_commandManager.IsRedoEnabled);
            _commandManager.Undo();
            Assert.IsTrue(_commandManager.IsRedoEnabled);
        }
    }
}