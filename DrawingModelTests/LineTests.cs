﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class LineTests
    {
        Line _line;

        // Test Initialize
        [TestInitialize()]
        public void Initialize()
        {
            _line = new Line();
        }

        // Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        // Test
        [TestMethod()]
        public void SetFirstPointTest()
        {
            _line.SetFirstPoint(0, 1);
            Assert.AreEqual(0, _line._x1);
            Assert.AreEqual(1, _line._y1);
        }

        // Test
        [TestMethod()]
        public void SetSecondPointTest()
        {
            _line.SetSecondPoint(2, 3);
            Assert.AreEqual(2, _line._x2);
            Assert.AreEqual(3, _line._y2);
        }

        // Test
        [TestMethod()]
        public void HighlightTest()
        {
            Assert.IsFalse(_line.highlight);
            _line.highlight = true;
            Assert.IsTrue(_line.highlight);
        }

        // Test
        [TestMethod()]
        public void DrawTest()
        {
            var mockGraphic = new MockGraphic();
            _line.SetFirstPoint(1, 2);
            _line.SetSecondPoint(3, 4);
            _line.Draw(mockGraphic);
            Assert.AreEqual(1, mockGraphic.GetPointList()[0].pointX);
            Assert.AreEqual(2, mockGraphic.GetPointList()[0].pointY);
            Assert.AreEqual(3, mockGraphic.GetPointList()[1].pointX);
            Assert.AreEqual(4, mockGraphic.GetPointList()[1].pointY);
        }

        // Test
        [TestMethod()]
        public void ClickCheckTest()
        {
            _line.SetFirstPoint(1, 1);
            _line.SetSecondPoint(3, 3);
            Assert.IsTrue(_line.ClickCheck(2, 2));
            Assert.IsFalse(_line.ClickCheck(5, 10));
        }

        // Test 
        [TestMethod()]
        public void DrawHighlightTest()
        {
            var mockGraphic = new MockGraphic();
            _line.SetFirstPoint(1, 2);
            _line.SetSecondPoint(3, 4);
            _line.highlight = true;
            _line.Draw(mockGraphic);
            Assert.AreEqual("Line : (1, 2, 3, 4)", mockGraphic.GetDrawString());
        }
    }
}