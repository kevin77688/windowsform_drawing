﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class PointTests
    {
        Point _point;
        // Initialize
        [TestInitialize()]
        public void Initialize()
        {
            _point = new Point(2, 5);
        }

        // Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        // Test
        [TestMethod()]
        public void PointTest()
        {
            Assert.AreEqual(2, _point.pointX);
            Assert.AreEqual(5, _point.pointY);
        }
    }
}