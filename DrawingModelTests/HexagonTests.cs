﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class HexagonTests
    {
        Hexagon _hexagon;

        // Test Initialize
        [TestInitialize()]
        public void Initialize()
        {
            _hexagon = new Hexagon();
        }

        // Test Cleanup
        [TestCleanup()]
        public void CleanUp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        // Test
        [TestMethod()]
        public void SetFirstPointTest()
        {
            _hexagon.SetFirstPoint(0, 1);
            Assert.AreEqual(0, _hexagon._x1);
            Assert.AreEqual(1, _hexagon._y1);
        }

        // Test
        [TestMethod()]
        public void SetSecondPointTest()
        {
            _hexagon.SetSecondPoint(2, 3);
            Assert.AreEqual(2, _hexagon._x2);
            Assert.AreEqual(3, _hexagon._y2);
        }

        // Test
        [TestMethod()]
        public void HighlightTest()
        {
            Assert.IsFalse(_hexagon.highlight);
            _hexagon.highlight = true;
            Assert.IsTrue(_hexagon.highlight);
        }

        // Test
        [TestMethod()]
        public void DrawTest()
        {
            var mockGraphic = new MockGraphic();
            _hexagon.SetFirstPoint(1, 1);
            _hexagon.SetSecondPoint(5, 5);
            _hexagon.Draw(mockGraphic);
            Assert.AreEqual(1, mockGraphic.GetPointList()[0].pointX);
            Assert.AreEqual(3, mockGraphic.GetPointList()[0].pointY);
            Assert.AreEqual(2, mockGraphic.GetPointList()[1].pointX);
            Assert.AreEqual(1, mockGraphic.GetPointList()[1].pointY);
            Assert.AreEqual(4, mockGraphic.GetPointList()[2].pointX);
            Assert.AreEqual(1, mockGraphic.GetPointList()[2].pointY);
            Assert.AreEqual(5, mockGraphic.GetPointList()[3].pointX);
            Assert.AreEqual(3, mockGraphic.GetPointList()[3].pointY);
            Assert.AreEqual(4, mockGraphic.GetPointList()[4].pointX);
            Assert.AreEqual(5, mockGraphic.GetPointList()[4].pointY);
            Assert.AreEqual(2, mockGraphic.GetPointList()[5].pointX);
            Assert.AreEqual(5, mockGraphic.GetPointList()[5].pointY);
        }

        // Test
        [TestMethod()]
        public void ClickCheckTest()
        {
            var mockGraphic = new MockGraphic();
            _hexagon.SetFirstPoint(1, 1);
            _hexagon.SetSecondPoint(5, 5);
            _hexagon.Draw(mockGraphic);
            Assert.IsTrue(_hexagon.ClickCheck(3, 3));
            Assert.IsFalse(_hexagon.ClickCheck(5, 5));
        }

        // Test 
        [TestMethod()]
        public void DrawHighlightTest()
        {
            var mockGraphic = new MockGraphic();
            _hexagon.SetFirstPoint(1, 1);
            _hexagon.SetSecondPoint(5, 5);
            _hexagon.highlight = true;
            _hexagon.Draw(mockGraphic);
            Assert.AreEqual("Hexagon : (1, 1, 5, 5)", mockGraphic.GetDrawString());
        }

        [TestMethod()]
        public void CloneTest()
        {
            Shape s = new Hexagon();
            s.SetFirstPoint(1, 1);
            s.SetSecondPoint(10, 10);
            Shape cloneS = s.Clone();
            Assert.AreEqual(typeof(Hexagon), s.GetType());
        }

        [TestMethod()]
        public void CheckPointTest()
        {
            Shape s = new Hexagon();
            var mockGraphic = new MockGraphic();
            s.SetFirstPoint(10, 10);
            s.SetSecondPoint(1, 1);
            s.highlight = true;
            s.Draw(mockGraphic);
            Assert.AreEqual(1, s._x1);
            Assert.AreEqual(1, s._y1);
            Assert.AreEqual(10, s._x2);
            Assert.AreEqual(10, s._y2);
        }
    }
}