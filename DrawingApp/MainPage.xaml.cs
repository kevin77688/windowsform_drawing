﻿using System.Collections.Generic;
using Windows.Foundation;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace DrawingApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        DrawingModel.Model _model;
        PresentationModel.PresentationModel _presentationModel;
        List<Button> _allShapeButton = new List<Button>();

        public MainPage()
        {
            this.InitializeComponent();
            ApplicationView.PreferredLaunchViewSize = new Size(1366, 768);
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.Maximized;
            TextBlock textBlock = new TextBlock();
            _model = new DrawingModel.Model();
            _presentationModel = new PresentationModel.PresentationModel(_model, _canvas);
            _lineButton.IsEnabled = true;
            _lineButton.Click += ChangeShape;
            _rectangleButton.IsEnabled = true;
            _rectangleButton.Click += ChangeShape;
            _hexagonButton.IsEnabled = true;
            _hexagonButton.Click += ChangeShape;
            _undoButton.IsEnabled = false;
            _undoButton.Click += UndoHandle;
            _redoButton.IsEnabled = false;
            _redoButton.Click += RedoHandle;
            _canvas.PointerPressed += HandleCanvasPressed;
            _canvas.PointerReleased += HandleCanvasReleased;
            _canvas.PointerMoved += HandleCanvasMoved;
            _canvas.Children.Add(textBlock);
            _clearButton.Click += HandleClearButtonClick;
            _model._modelChanged += HandleModelChanged;
            _allShapeButton.Add(_lineButton);
            _allShapeButton.Add(_rectangleButton);
            _allShapeButton.Add(_hexagonButton);
        }

        // While click clear button
        private void HandleClearButtonClick(object sender, RoutedEventArgs e)
        {
            _model.Clear();
        }

        // While canvas pressed
        public void HandleCanvasPressed(object sender, PointerRoutedEventArgs e)
        {
            _model.CheckPointerPressed(e.GetCurrentPoint(_canvas).Position.X, e.GetCurrentPoint(_canvas).Position.Y);
        }

        // While move mouse
        public void HandleCanvasMoved(object sender, PointerRoutedEventArgs e)
        {
            _model.CheckPointerMoved(e.GetCurrentPoint(_canvas).Position.X, e.GetCurrentPoint(_canvas).Position.Y);
        }

        // While release mouse
        public void HandleCanvasReleased(object sender, PointerRoutedEventArgs e)
        {
            _model.CheckPointerReleased();
            foreach (Button button in _allShapeButton)
                button.IsEnabled = true;
            _model.SetState(DrawingModel.Constant.SELECTION);
        }

        // Change button action
        private void ChangeShape(object sender, RoutedEventArgs e)
        {
            Button currentButton = (Button)sender;
            _model.SetState((string)currentButton.Content);
            foreach (Button button in _allShapeButton)
                button.IsEnabled = true;
            currentButton.IsEnabled = false;
        }

        // Undoo button action
        public void UndoHandle(object sender, RoutedEventArgs e)
        {
            _model.UndoButton();
            HandleModelChanged();
        }

        // Redo button action
        public void RedoHandle(object sender, RoutedEventArgs e)
        {
            _model.RedoButton();
            HandleModelChanged();
        }

        // Check Model Changed
        public void HandleModelChanged()
        {
            _redoButton.IsEnabled = _model.IsRedoEnabled;
            _undoButton.IsEnabled = _model.IsUndoEnabled;
            _clearButton.IsEnabled = _model.IsClearButtonEnable;
            _presentationModel.Draw();
        }
    }
}
