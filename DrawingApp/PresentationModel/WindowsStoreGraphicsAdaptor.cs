﻿using System;
using System.Collections.Generic;
using Windows.UI;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml.Media;
using DrawingModel;
using Windows.UI.Xaml;

namespace DrawingApp.PresentationModel
{
    public class WindowsStoreGraphicsAdaptor : IGraphics
    {
        Canvas _canvas;
        String _currentString;

        public WindowsStoreGraphicsAdaptor(Canvas canvas)
        {
            this._canvas = canvas;
        }

        // Action while clear all
        public void ClearAll()
        {
            _canvas.Children.Clear();
        }

        // Action while use DrawLine
        public void DrawLine(List<Point> pointList, bool highlight)
        {
            Windows.UI.Xaml.Shapes.Line line = new Windows.UI.Xaml.Shapes.Line();
            line.X1 = pointList[0].pointX;
            line.Y1 = pointList[0].pointY;
            line.X2 = pointList[1].pointX;
            line.Y2 = pointList[1].pointY;
            if (highlight)
            {
                DoubleCollection collection = new DoubleCollection();
                collection.Add(Constant.NUMBER_FOUR);
                collection.Add(Constant.NUMBER_TWO);
                line.StrokeDashArray = collection;
                line.StrokeThickness = Constant.NUMBER_TWO;
                DrawDot(pointList[0]);
                DrawDot(pointList[1]);
            }
            line.Stroke = new SolidColorBrush(Colors.Black);
            _canvas.Children.Add(line);
        }

        // Action while use Draw Rectangle
        public void DrawRectangle(List<Point> pointList, bool highlight)
        {
            DrawPolygon(Constant.NUMBER_FOUR, pointList, new SolidColorBrush(Colors.Red), highlight);
        }

        // Action while use Draw Hexagon
        public void DrawHexagon(List<Point> pointList, bool highlight)
        {
            DrawPolygon(Constant.NUMBER_SIX, pointList, new SolidColorBrush(Colors.LightBlue), highlight);
        }

        // Action while use Draw Polygon
        public void DrawPolygon(int dimension, List<Point> pointList, SolidColorBrush colors, bool highlight)
        {
            Polygon shape = new Polygon();
            PointCollection shapePoint = new PointCollection();
            for (int index = 0; index < dimension; index++)
                shapePoint.Add(new Windows.Foundation.Point(pointList[index].pointX, pointList[index].pointY));
            shape.Points = shapePoint;
            if (highlight)
                HighlightShape(pointList);
            else
                shape.Fill = colors;
            _canvas.Children.Add(shape);
        }

        // Action while draw string
        public void DrawString(string getString)
        {
            TextBlock textBlock = new TextBlock();
            textBlock.Text = getString;
            _canvas.Children.Add(textBlock);
        }

        // Action while select highlight shape
        private void HighlightShape(List<Point> pointList)
        {
            Polygon shape = new Polygon();
            PointCollection shapePoint = new PointCollection();
            Point maximumPoint = FindMaximum(pointList);
            Point minimumPoint = FindMinimum(pointList);
            shapePoint.Add(new Windows.Foundation.Point(minimumPoint.pointX, minimumPoint.pointY));
            shapePoint.Add(new Windows.Foundation.Point(maximumPoint.pointX, minimumPoint.pointY));
            shapePoint.Add(new Windows.Foundation.Point(maximumPoint.pointX, maximumPoint.pointY));
            shapePoint.Add(new Windows.Foundation.Point(minimumPoint.pointX, maximumPoint.pointY));
            shape.Points = shapePoint;
            DoubleCollection collection = new DoubleCollection();
            collection.Add(Constant.NUMBER_FOUR);
            collection.Add(Constant.NUMBER_TWO);
            shape.StrokeDashArray = collection;
            shape.StrokeThickness = Constant.NUMBER_TWO;
            shape.Stroke = new SolidColorBrush(Colors.Black);
            _canvas.Children.Add(shape);
            DrawDashRectangle(minimumPoint, maximumPoint);
        }

        // Draw dash rectangle
        private void DrawDashRectangle(Point minimumPoint, Point maximumPoint)
        {
            DrawDot(new Point(minimumPoint.pointX, minimumPoint.pointY));
            DrawDot(new Point(maximumPoint.pointX, minimumPoint.pointY));
            DrawDot(new Point(maximumPoint.pointX, maximumPoint.pointY));
            DrawDot(new Point(minimumPoint.pointX, maximumPoint.pointY));
        }

        // Draw white dot
        private void DrawDot(Point point)
        {
            Ellipse circle = new Ellipse();
            circle.Width = Constant.NUMBER_TEN;
            circle.Height = Constant.NUMBER_TEN;
            circle.Margin = new Thickness(point.pointX - Constant.NUMBER_FIVE, point.pointY - Constant.NUMBER_FIVE, 0, 0);
            circle.Fill = new SolidColorBrush(Colors.White);
            circle.Stroke = new SolidColorBrush(Colors.Black);
            _canvas.Children.Add(circle);
        }

        // Find maximum point
        private Point FindMaximum(List<Point> pointList)
        {
            float pointX = (float)pointList[0].pointX;
            float pointY = (float)pointList[0].pointY;
            foreach (Point point in pointList)
            {
                if (point.pointX > pointX)
                    pointX = (float)point.pointX;
                if (point.pointY > pointY)
                    pointY = (float)point.pointY;
            }
            return new Point(pointX, pointY);
        }

        // Find minimum point
        private Point FindMinimum(List<Point> pointList)
        {
            float pointX = (float)pointList[0].pointX;
            float pointY = (float)pointList[0].pointY;
            foreach (Point point in pointList)
            {
                if (point.pointX < pointX)
                    pointX = (float)point.pointX;
                if (point.pointY < pointY)
                    pointY = (float)point.pointY;
            }
            return new Point(pointX, pointY);
        }
    }
}
