﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using DrawingModel;

namespace DrawingApp.PresentationModel
{
    public class PresentationModel
    {
        Model _model;
        IGraphics _iGraphics;
        public PresentationModel(Model model, Canvas canvas)
        {
            this._model = model;
            _iGraphics = new WindowsStoreGraphicsAdaptor(canvas);
        }

        // Draw presentation model
        public void Draw()
        {
            // 重複使用igraphics物件
            _model.DrawAllShape(_iGraphics);
        }

        // True to False, False to True
        public bool ChangeStatus(bool set)
        {
            return !set;
        }
    }
}
